# ktr-msc-ls1

### Specifications
- <a href="docs/specifications.pdf">docs/specifications.pdf</a>

### Requirements

- Node Js (https://nodejs.org/en/)

### Compile

```sh
npm install
```

### Run

```sh
npm run start -o
```

## Reason for technical choices

Given the time I had to do this test, I chose to use the Angular (https://angular.io/) front-end framework.

I chose this framework because it is the front-end framework that I master the best and that I used recently.

Here is the set of features I managed to implement,

- User registration
- Login
- Edit profile
- Logout
- Add card
- Viewing cards
- Editing a card
- Delete a card
- Data persistence
- Multi-users

The project being totally realized with angular, I decided to store the data in the localstorage (https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage) in order to get results quickly and to persist the data.

### TODO

- Improvement of the user interface;
- Refactoring of the code (duplicates in the css, and some methods can be generic);
- Add a backend to manage data efficiently;
- Allow exchange between multiple users;
- Password should not be in plain text !
- Close popups after editing;
- ...

## Screenshots 

![Login](docs/screenshots/login.png)
![Signup](docs/screenshots/signup.png)
![Profile](docs/screenshots/profile.png)
![Add Card](docs/screenshots/add_card.png)

