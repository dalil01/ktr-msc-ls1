import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { UserType } from "../../types/models/user.type";

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  private authUserKey: string = "authUser";
  private authUserInLocalStorageSubject: Subject<string> = new Subject<string>();

  constructor() {
  }

  public getAuthUserInLocalStorageSubject(): Subject<string> {
    return this.authUserInLocalStorageSubject;
  }

  public setAuthUserInLocalStorage(user: UserType): void {
    const stringyUser = JSON.stringify(user)
    localStorage.setItem(this.authUserKey, stringyUser);
    this.authUserInLocalStorageSubject.next(stringyUser);
  }

  public removeAuthUserFromLocalStorage(): void {
    localStorage.removeItem(this.authUserKey);
    this.authUserInLocalStorageSubject.next("");
  }

  public getAuthUserFromLocalStorage(): UserType {
    // @ts-ignore
    return JSON.parse(localStorage.getItem(this.authUserKey)) as UserType;
  }

  public isAuth(): boolean {
    return localStorage.getItem(this.authUserKey) != null;
  }

}
