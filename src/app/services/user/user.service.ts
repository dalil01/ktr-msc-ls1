import { Injectable } from '@angular/core';
import { UserType } from "../../types/models/user.type";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private localStorageKey: string = "users";

  constructor() { }

  public addUser(user : UserType): void {
    const userExist = this.getUserByEmail(user.email);

    if (!userExist) {
      const users = this.getUsers();
      users.push(user);
      localStorage.setItem(this.localStorageKey, JSON.stringify(users));
    }
  }

  public updateUser(authUser : UserType): UserType | undefined {
    const users = this.getUsers();

    for (let i = 0; i < users.length; i++) {
      if (users[i].email == authUser.email) {
        users[i] = authUser;
        localStorage.setItem(this.localStorageKey, JSON.stringify(users));
        break;
      }
    }

    return authUser;
  }

  public getUserByEmail(email : string): UserType | undefined {
    const users = this.getUsers();

    let foundUser;
    for (let user of users) {
      if (user.email == email) {
        foundUser = user;
        break;
      }
    }

    return foundUser;
  }

  public getUserByEmailPassword(email : string, password: string): UserType | undefined {
    const users = this.getUsers();

    let foundUser;
    for (let user of users) {
      if (user.email == email && user.password == password) {
        foundUser = user;
        break;
      }
    }

    return foundUser;
  }

  public getUsers(): UserType[] {
    // @ts-ignore
    return JSON.parse(localStorage.getItem(this.localStorageKey)) || [];
  }

}
