import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from "@angular/forms";

import { AppComponent } from './components/app.component';
import { ProfileComponent } from './components/profile/profile.component';
import { PortalComponent } from "./components/portal/portal.component";
import { AddCardComponent } from './components/profile/add-card/add-card.component';
import { EditCardComponent } from './components/profile/edit-card/edit-card.component';
import { DeleteCardComponent } from './components/profile/delete-card/delete-card.component';
import { EditProfileComponent } from './components/profile/edit-profile/edit-profile.component';

import { UserService } from "./services/user/user.service";
import { SecurityService } from "./services/security/security.service";

@NgModule({
  declarations: [
    AppComponent,
    PortalComponent,
    ProfileComponent,
    AddCardComponent,
    EditCardComponent,
    DeleteCardComponent,
    EditProfileComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [
    UserService,
    SecurityService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
