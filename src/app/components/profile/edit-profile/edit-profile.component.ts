import { Component, Input, OnInit } from '@angular/core';
import { UserType } from "../../../types/models/user.type";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { UserService } from "../../../services/user/user.service";
import { SecurityService } from "../../../services/security/security.service";

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

  @Input() authUser!: UserType;

  public editProfileForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private securityService: SecurityService, private userService: UserService) { }

  ngOnInit(): void {
    this.initEditProfileForm();
  }

  private initEditProfileForm(): void {
    this.editProfileForm = this.formBuilder.group({
      name: new FormControl(this.authUser.name),
      companyName: new FormControl(this.authUser.companyName),
      email: new FormControl(this.authUser.email, [Validators.email]),
      phoneNumber: new FormControl(this.authUser.phoneNumber),
      password: new FormControl(this.authUser.password)
    });
  }

  onSubmitEditProfileForm(): void {
    if (this.editProfileForm.valid) {
      this.authUser.name = this.editProfileForm.value.name;
      this.authUser.companyName = this.editProfileForm.value.companyName;
      this.authUser.email = this.editProfileForm.value.email;
      this.authUser.phoneNumber = this.editProfileForm.value.phoneNumber;
      this.authUser.password = this.editProfileForm.value.password;
      this.securityService.setAuthUserInLocalStorage(this.authUser);
      this.userService.updateUser(this.authUser);
    }
  }

}
