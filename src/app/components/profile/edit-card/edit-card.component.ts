import { Component, Input, OnInit } from '@angular/core';
import { UserType } from "../../../types/models/user.type";
import { CardType } from "../../../types/models/card.type";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { SecurityService } from "../../../services/security/security.service";
import { UserService } from "../../../services/user/user.service";

@Component({
  selector: 'app-edit-card',
  templateUrl: './edit-card.component.html',
  styleUrls: ['./edit-card.component.css']
})
export class EditCardComponent implements OnInit {

  @Input() authUser!: UserType;
  @Input() currentCard!: CardType;

  public editCardForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private securityService: SecurityService, private userService: UserService) { }

  ngOnInit(): void {
    this.initEditCardFormForm();
  }

  private initEditCardFormForm(): void {
    this.editCardForm = this.formBuilder.group({
      name: new FormControl(this.currentCard.name),
      companyName: new FormControl(this.currentCard.companyName),
      email: new FormControl(this.currentCard.email, [Validators.email]),
      phoneNumber: new FormControl(this.currentCard.phoneNumber),
    });
  }

  onSubmitEditCardForm() {
    if (this.editCardForm.valid) {
      this.currentCard.name = this.editCardForm.value.name;
      this.currentCard.companyName = this.editCardForm.value.companyName;
      this.currentCard.email = this.editCardForm.value.email;
      this.currentCard.phoneNumber = this.editCardForm.value.phoneNumber;
      this.securityService.setAuthUserInLocalStorage(this.authUser);
      this.userService.updateUser(this.authUser);
    }
  }

}
