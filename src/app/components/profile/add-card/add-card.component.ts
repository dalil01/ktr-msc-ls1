import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { UserType } from "../../../types/models/user.type";
import { CardType } from "../../../types/models/card.type";
import { UserService } from "../../../services/user/user.service";
import { SecurityService } from "../../../services/security/security.service";

@Component({
  selector: 'app-add-card',
  templateUrl: './add-card.component.html',
  styleUrls: ['./add-card.component.css']
})
export class AddCardComponent implements OnInit {

  @Input() authUser!: UserType;
  addCardForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private securityService: SecurityService, private userService: UserService) { }

  ngOnInit(): void {
    this.initAddCardFormForm();
  }

  private initAddCardFormForm(): void {
    this.addCardForm = this.formBuilder.group({
      name: new FormControl(''),
      companyName: new FormControl(''),
      email: new FormControl('', [Validators.email]),
      phoneNumber: new FormControl(''),
    });
  }

  onSubmitAddCardForm() {
    if (this.addCardForm.valid) {
      const card: CardType = {
        name: this.addCardForm.value.name,
        companyName: this.addCardForm.value.companyName,
        email: this.addCardForm.value.email,
        phoneNumber: this.addCardForm.value.phoneNumber
      };
      this.authUser.cards.push(card);
      this.securityService.setAuthUserInLocalStorage(this.authUser);
      this.userService.updateUser(this.authUser);
      this.addCardForm.reset();
    }
  }

}
