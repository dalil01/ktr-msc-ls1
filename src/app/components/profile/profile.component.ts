import { Component, Input, OnInit } from '@angular/core';
import { UserType } from "../../types/models/user.type";
import { CardType } from "../../types/models/card.type";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  @Input() authUser!: UserType;

  public modalIsOpen = false;
  public modalContent = "";

  public currentCard!: CardType;

  constructor() { }

  ngOnInit(): void {
  }

  public openModal(modalContent: string): void {
    this.modalContent = modalContent;
    this.modalIsOpen = true;
  }

  public closeModal(): void {
    this.modalIsOpen = false;
  }

}
