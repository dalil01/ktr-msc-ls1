import { Component, Input, OnInit } from '@angular/core';
import { UserType } from "../../../types/models/user.type";
import { CardType } from "../../../types/models/card.type";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { SecurityService} from "../../../services/security/security.service";
import { UserService } from "../../../services/user/user.service";

@Component({
  selector: 'app-delete-card',
  templateUrl: './delete-card.component.html',
  styleUrls: ['./delete-card.component.css']
})
export class DeleteCardComponent implements OnInit {

  @Input() authUser!: UserType;
  @Input() currentCard!: CardType;

  public deleteCardForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private securityService: SecurityService, private userService: UserService) { }

  ngOnInit(): void {
    this.initDeleteCardForm();
  }

  private initDeleteCardForm(): void {
    this.deleteCardForm = this.formBuilder.group({
      password: new FormControl('')
    });
  }

  onSubmitDeleteCardForm(): void {
    if (this.deleteCardForm.valid) {
      if (this.authUser.password == this.deleteCardForm.value.password) {
        this.authUser.cards = this.authUser.cards.filter((card) => card.email != this.currentCard.email);
        this.securityService.setAuthUserInLocalStorage(this.authUser);
        this.userService.updateUser(this.authUser);
      }
    }
  }

}
