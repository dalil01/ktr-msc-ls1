import {Component, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {SecurityService} from "../services/security/security.service";
import {UserType} from "../types/models/user.type";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  isAuth = false;
  authUser!: UserType;
  isAuthSubscription!: Subscription;

  public modalIsOpen = false;
  public modalContent = "";

  constructor(private securityService: SecurityService) {
  }

  public openModal(modalContent: string): void {
    this.modalContent = modalContent;
    this.modalIsOpen = true;
  }

  public closeModal(): void {
    this.modalIsOpen = false;
  }

  ngOnInit() {
    this.isAuth = this.securityService.isAuth();
    this.authUser = this.securityService.getAuthUserFromLocalStorage();
    this.isAuthSubscription = this.securityService.getAuthUserInLocalStorageSubject().subscribe(() => {
      this.isAuth = this.securityService.isAuth();
      this.authUser = this.securityService.getAuthUserFromLocalStorage();
    });
  }

  logOut(): void {
    this.securityService.removeAuthUserFromLocalStorage();
  }

  ngOnDestroy(): void {
    this.isAuthSubscription?.unsubscribe();
  }

}
