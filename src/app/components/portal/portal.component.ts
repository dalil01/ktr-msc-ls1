import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { AlertType } from "../../types/alert/alert.type";

import { UserService } from "../../services/user/user.service";
import { UserType } from "../../types/models/user.type";
import { SecurityService } from "../../services/security/security.service";

@Component({
  selector: 'app-portal',
  templateUrl: './portal.component.html',
  styleUrls: ['./portal.component.css']
})
export class PortalComponent implements OnInit {

  public signUpForm!: FormGroup;
  public loginForm!: FormGroup;

  public showAlert: boolean = false;
  public alert!: AlertType;
  public successMessage : string = "Congrats, You've successfully signed up to ktr-msc-ls1 !";
  public errorMessage : string = "ERROR something went wrong try again.";
  public invalidFormErrorMessage : string = "The form is not valid.";

  constructor(private formBuilder: FormBuilder, private userService: UserService, private securityService: SecurityService) { }

  ngOnInit(): void {
    this.initLoginForm();
    this.initSignUpForm();
  }

  private initLoginForm(): void {
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', [Validators.email]),
      password: new FormControl('')
    });
  }

  private initSignUpForm(): void {
    this.signUpForm = this.formBuilder.group({
      name: new FormControl(''),
      companyName: new FormControl(''),
      email: new FormControl('', [Validators.email]),
      phoneNumber: new FormControl(''),
      password: new FormControl('')
    });
  }

  onSubmitLoginForm(): void {
    if (this.loginForm.valid) {
      const userExist = this.userService.getUserByEmailPassword(this.loginForm.value.email, this.loginForm.value.password);

      if (userExist) {
        this.securityService.setAuthUserInLocalStorage(userExist);
      } else {
        this.showAlertMsg("ERROR", "Invalid credentials !");
      }

      this.loginForm.reset();
    } else {
      this.showAlertMsg("ERROR", this.invalidFormErrorMessage);
    }
  }

  onSubmitSignUpForm(): void {
    if (this.signUpForm.valid) {
      const user: UserType = {
         name: this.signUpForm.value.name,
         companyName: this.signUpForm.value.companyName,
         email: this.signUpForm.value.email,
         phoneNumber: this.signUpForm.value.phoneNumber,
         password: this.signUpForm.value.password,
         cards: []
      };

      this.userService.addUser(user);

      if (this.userService.getUserByEmailPassword(user.email, user.password)) {
        this.showAlertMsg("SUCCESS");
        this.signUpForm.reset();
      } else {
        this.showAlertMsg("ERROR");
      }
    } else {
      this.showAlertMsg("ERROR", this.invalidFormErrorMessage);
    }
  }

  private showAlertMsg(type: "ERROR" | "SUCCESS", personalizedMessage: string = ""): void {
    this.showAlert = true;

    if (type == "SUCCESS") {
      this.alert = {type: "SUCCESS", message: (personalizedMessage == '') ? this.successMessage : personalizedMessage};
    } else {
      this.alert = {type: "ERROR", message: (personalizedMessage == '') ? this.errorMessage : personalizedMessage};
    }

    setTimeout(() => {
      this.showAlert = false;
    }, 6000);
  }

}
