export type AlertType = {
  type: undefined | 'ERROR' | 'SUCCESS',
  message: string | null
}
