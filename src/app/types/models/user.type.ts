import { CardType } from "./card.type";

export type UserType = {
  name: string,
  companyName?: string,
  email: string,
  phoneNumber?: number,
  password: string,
  cards: CardType[]
}
