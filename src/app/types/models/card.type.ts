export type CardType = {
  name?: string,
  companyName?: string,
  email: string,
  phoneNumber?: number
}
